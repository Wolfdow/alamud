# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import TalkWithEvent

class TalkWithAction(Action2):
    EVENT = TalkWithEvent
    ACTION = "talk-with"
    RESOLVE_OBJECT = "resolve_for_operate"
