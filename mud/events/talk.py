# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class TalkWithEvent(Event2):
    NAME = "talk-with"

    def perform(self):
        if not self.object.has_prop("talkable-to"):
            self.fail()
            return self.inform("talk-with.failed")
        self.inform("talk-with")