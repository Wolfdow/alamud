# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .effect import Effect2
from mud.events import TalkWithEvent

class TalkWithEffect(Effect2):
    EVENT = TalkWithEvent
